from django.apps import AppConfig


class DappadavidscrumyConfig(AppConfig):
    name = 'dappadavidscrumy'
